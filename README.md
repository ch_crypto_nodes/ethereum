# Ethereum

An Ethereum docker image.

## Tags

- `v1.10.15` ([ethereum/client-go:v1.10.15](https://hub.docker.com/r/ethereum/client-go/tags))
- `v1.10.3` ([ethereum/client-go:v1.10.3](https://hub.docker.com/r/ethereum/client-go/tags))
- `v1.10.1` ([ethereum/client-go:v1.10.1](https://hub.docker.com/r/ethereum/client-go/tags))

## What is Ethereum?

Ethereum is a reference client that implements the Ethereum protocol for remote procedure call (RPC) use. Learn more about Ethereum on the [Ethereum site](https://geth.ethereum.org/).

## Usage

### How to use this image

You must run a container with predefined environment variables.

Create `.env` file with following variables:

| Env variable                    | Description                                                                |
| ------------------------------- | -------------------------------------------------------------------------- |
| `PATH_TO_DATA_FOLDER`           | Full path to the blockchain data folder.                                   |


Run container: 

```bash
docker-compose up -d 
```

Stop container: 

```bash
docker-compose down -v
```

### Basic Auth

Login and password from the `.htpasswd` file
